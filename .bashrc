# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !

# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

# Put your fun stuff here.

alias youtube-dl-audio='youtube-dl -x --audio-format mp3 --audio-quality 0'
alias jekyll-pro-build='JEKYLL_ENV=production jekyll build'

export GEM_HOME=$HOME/.gem
export PATH=$HOME/.gem/bin:$PATH
export PATH=$HOME/.local/bin:$PATH

#Perl local::lib
PATH="/home/christian/.perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/christian/.perl5/lib/.perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/christian/.perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/christian/.perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/christian/.perl5"; export PERL_MM_OPT;


# TMUX
if which tmux >/dev/null 2>&1; then
    #if not inside a tmux session, and if no session is started, start a new session
    test -z "$TMUX" && (tmux attach || tmux new-session)
fi

# Powerline
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. "$HOME/.local/lib64/python3.6/site-packages/powerline/bindings/bash/powerline.sh"
